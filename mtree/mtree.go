package mtree

import (
	"bitbucket.org/taringa/imageid/imhash"
	"sort"
	"sync"
)

const leafCapacity = 16

type MTree struct {
	mu   sync.RWMutex
	data nodeData
}

type nodeData interface {
	add(tree *MTree, hash imhash.ImageHash) *MTree
	search(tree *MTree, hash imhash.ImageHash, d int) (*imhash.ImageHash, int, int64)
}

type forkNode struct {
	hash      imhash.ImageHash
	nearDist  int
	near, far *MTree
}

type leafNode struct {
	amount int
	hashes [leafCapacity]imhash.ImageHash
}

func New() *MTree {
	return &MTree{data: &leafNode{}}
}

func (tree *MTree) Add(hash imhash.ImageHash) int64 {
	depth := int64(0)
	for tree != nil {
		depth++
		func() {
			tree.mu.Lock()
			defer tree.mu.Unlock()
			tree = tree.data.add(tree, hash)
		}()
	}
	return depth
}

func newLeafNode(hash imhash.ImageHash) *leafNode {
	leaf := &leafNode{}
	leaf.amount = 1
	leaf.hashes[0] = hash
	return leaf
}

func (fork *forkNode) add(tree *MTree, hash imhash.ImageHash) *MTree {
	if hash.Distance(fork.hash) <= fork.nearDist {
		if fork.near == nil {
			fork.near = &MTree{data: newLeafNode(hash)}
			return nil
		}
		return fork.near
	}
	if fork.far == nil {
		fork.far = &MTree{data: newLeafNode(hash)}
		return nil
	}
	return fork.far
}

func (leaf *leafNode) add(tree *MTree, hash imhash.ImageHash) *MTree {
	if leaf.amount < leafCapacity {
		leaf.hashes[leaf.amount] = hash
		leaf.amount++
		return nil
	}
	fork := leaf.split()
	tree.data = fork
	return fork.add(tree, hash)
}

func (leaf *leafNode) split() *forkNode {
	nearDist, near, far := partition(leaf.hashes[0], leaf.hashes[1:])
	return &forkNode{
		hash:     leaf.hashes[0],
		nearDist: nearDist,
		near:     &MTree{data: leafNodeFromSlice(near)},
		far:      &MTree{data: leafNodeFromSlice(far)},
	}
}

func leafNodeFromSlice(hashes []imhash.ImageHash) *leafNode {
	leaf := &leafNode{}
	leaf.amount = len(hashes)
	copy(leaf.hashes[:], hashes)
	return leaf
}

type HashDist struct {
	hashes    []imhash.ImageHash
	distances []int
}

func (hd HashDist) Len() int { return len(hd.hashes) }
func (hd HashDist) Swap(i, j int) {
	hd.hashes[i], hd.hashes[j] = hd.hashes[j], hd.hashes[i]
	hd.distances[i], hd.distances[j] = hd.distances[j], hd.distances[i]
}
func (hd HashDist) Less(i, j int) bool { return hd.distances[i] < hd.distances[j] }

func partition(hash imhash.ImageHash, rest []imhash.ImageHash) (int, []imhash.ImageHash, []imhash.ImageHash) {
	hd := HashDist{rest, make([]int, len(rest))}
	for i := range rest {
		hd.distances[i] = hash.Distance(hd.hashes[i])
	}
	sort.Sort(hd)
	middle := len(hd.hashes) / 2
	return hd.distances[middle], hd.hashes[:middle+1], hd.hashes[middle+1:]
}

func (tree *MTree) SearchClosest(hash imhash.ImageHash, d int) (found bool, closestHash imhash.ImageHash, dist int, nodesVisited int64) {
	closest, dist, nodesVisited := tree.data.search(tree, hash, d)
	found = closest != nil
	if found {
		closestHash = *closest
	}
	return
}

func (leaf *leafNode) search(tree *MTree, hash imhash.ImageHash, d int) (*imhash.ImageHash, int, int64) {
	tree.mu.RLock()
	defer tree.mu.RUnlock()

	nodesVisited := int64(0)
	var closest *imhash.ImageHash

	for i := 0; i < leaf.amount; i++ {
		dist := hash.Distance(leaf.hashes[i])
		nodesVisited++
		if (closest == nil && dist <= d) || dist < d {
			d = dist
			closest = &leaf.hashes[i]
			if dist == 0 {
				break
			}
		}
	}
	return closest, d, nodesVisited
}

func (fork *forkNode) search(tree *MTree, hash imhash.ImageHash, d int) (*imhash.ImageHash, int, int64) {
	tree.mu.RLock()
	defer tree.mu.RUnlock()

	dist := hash.Distance(fork.hash)
	nodesVisited := int64(1)

	var closest *imhash.ImageHash
	if dist <= d {
		d = dist
		closest = &fork.hash
		if dist == 0 {
			return closest, d, nodesVisited
		}
	}

	if dist-d <= fork.nearDist {
		nearClosest, nearD, nearNodesVisited := fork.near.data.search(fork.near, hash, d)
		nodesVisited += nearNodesVisited
		if nearClosest != nil {
			if (closest == nil && nearD <= d) || nearD < d {
				closest = nearClosest
				d = nearD
			}
		}
	}
	if dist+d > fork.nearDist {
		farClosest, farD, farNodesVisited := fork.far.data.search(fork.far, hash, d)
		nodesVisited += farNodesVisited
		if (closest == nil && farD <= d) || farD < d {
			closest = farClosest
			d = farD
		}
	}
	return closest, d, nodesVisited
}
