package imageid

import (
	"bitbucket.org/taringa/imageid/imhash"
	"bitbucket.org/taringa/imageid/log"
	"bitbucket.org/taringa/imageid/stats"
	"image"
	_ "image/gif"  // add support for loading GIF images
	_ "image/jpeg" // add support for loading JPEG images
	_ "image/png"  // add support for loading PNG images
	"os"
)

type URLPair struct{ URL, Canonical string }

func validateFormat(filename string) string {
	file, err := os.Open(filename)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	_, format, err := image.DecodeConfig(file)
	if err != nil {
		panic(err)
	}
	return format
}

func Process(db *DB, workerID string, filename string, url string, similarChan chan<- *URLPair) {
	log.Debugf("Processing %s", url)

	stats.SetWorker(workerID, "Querying: "+url)
	canonical := db.getCanonicalForURL(url)
	if canonical != "" {
		log.Debugf("Already indexed. Canonical: %s", canonical)
		return
	}

	stats.URLProcessed()

	stats.SetWorker(workerID, "Verifying image format: "+url)
	validateFormat(filename)

	stats.SetWorker(workerID, "Querying MD5: "+url)
	md5, canonical := db.queryMd5(filename)
	if canonical != "" {
		log.Debugf("MD5 duplicate: %s %s", url, canonical)
		log.OnErrorPanic(db.addURL(nil, url, canonical))
		return
	}

	addHash, hash, canonical := checkHash(db, workerID, filename, url, similarChan)

	save(db, workerID, url, canonical, md5, addHash, hash)
}

func checkHash(db *DB, workerID, filename, url string, similarChan chan<- *URLPair) (bool, imhash.ImageHash, string) {
	var hash imhash.ImageHash

	stats.SetWorker(workerID, "Calculating hash:"+url)
	hash, err := imhash.FromFile(filename)
	if err != nil {
		log.Debugf("Could not calculate hash %s: %s", url, err)
		return false, hash, url
	}
	log.Debugf("Calculated hash %s: %s", url, hash)

	stats.SetWorker(workerID, "Querying hash: "+url)
	found, hash, canonical, dist := db.queryHash(url, hash)
	if !found {
		log.Debugf("New hash node: %s", url)
		return true, hash, url
	}

	if canonical == "" {
		panic("Database inconsistency: " + url)
	}

	log.Debugf("hash distance %d: %s %s", dist, url, canonical)
	if dist > 0 {
		if similarChan != nil {
			similarChan <- &URLPair{url, canonical}
		}
		log.OnErrorPanic(db.Store.LogSimilar(url, canonical))
	}
	return false, hash, canonical
}

func save(db *DB, workerID, url, canonical, md5 string, addHash bool, hash imhash.ImageHash) {
	tx := db.Store.StartTx()
	if tx == nil {
		panic("Could not start transaction")
	}
	defer tx.End()

	stats.SetWorker(workerID, "Adding URL: "+url)
	log.OnErrorPanic(db.addURL(tx, url, canonical))
	stats.SetWorker(workerID, "Adding MD5: "+url)
	log.OnErrorPanic(db.addMd5(tx, md5, canonical))
	if addHash {
		stats.SetWorker(workerID, "Adding hash: "+canonical)
		log.OnErrorPanic(db.addHash(tx, hash, canonical))
	}
}
