/*
imhash computes the 128-bit hash (dhash+phash) of an image.
*/
package main

/*
#cgo CPPFLAGS: -I. -ffast-math -O3
#cgo LDFLAGS: -lpng -ljpeg -lm -lpthread -lfftw3
#include "imhash.h"
extern int imhash(const char *file, imhash_t *hash);
*/
import "C"

import (
	"fmt"
	"os"
	"unsafe"
)

func main() {
	if len(os.Args) <= 1 {
		fmt.Fprintf(os.Stderr, "Usage: imhash <file>\n")
		os.Exit(1)
	}

	filename := C.CString(os.Args[1])
	defer C.free(unsafe.Pointer(filename))

	hash := C.struct__imhash_t{}
	err := C.imhash(filename, &hash)
	if int(err) != 0 {
		os.Exit(int(err))
	}

	fmt.Printf("%016x%016x\n", uint64(hash.dhash), uint64(hash.phash))
}
