package server

import (
	"bitbucket.org/taringa/imageid"
	"bitbucket.org/taringa/imageid/stats"
	"encoding/json"
	"net/http"
	"strings"
)

func getURLParam(r *http.Request) string {
	q := r.URL.RawQuery
	prefix := "url="
	if !strings.HasPrefix(q, prefix) {
		return ""
	}
	url := q[len(prefix):]
	return url
}

func jsonResponse(w http.ResponseWriter, v interface{}) {
	js, err := json.Marshal(v)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}

func canonicalHandler(db *imageid.DB) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		stats.CanonicalRequested()

		url := getURLParam(r)
		if url == "" {
			http.Error(w, "Missing url query parameter", http.StatusBadRequest)
			return
		}

		canonical := db.Query(url)
		stats.URLQueried(canonical != "")
		jsonResponse(w, struct{ Canonical string }{canonical})
	})
}

func processURLHandler(urlsQueue chan<- string) http.HandlerFunc {
	urlAdded := make(chan string)

	go func() {
		var urls []string
		stats.RegisterFunc(
			"buffer_urls",
			func() interface{} { return len(urls) },
		)

		for {
			select {
			case url := <-urlAdded:
				urls = append(urls, url)
				for len(urls) > 0 {
					select {
					case urlsQueue <- urls[0]:
						urls = urls[1:]
					case url := <-urlAdded:
						urls = append(urls, url)
					case <-imageid.Shutdown:
						return
					}
				}
			case <-imageid.Shutdown:
				return
			}
		}
	}()

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Method != "POST" {
			http.NotFound(w, r)
			return
		}

		url := getURLParam(r)
		if url == "" {
			http.Error(w, "Missing url query parameter", http.StatusBadRequest)
			return
		}

		urlAdded <- url
		jsonResponse(w, "Added "+url)
	})
}
