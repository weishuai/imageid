package kvstore

type KeyValueStore interface {
	Get(table string, k string) (string, error)
	Add(tx Tx, table string, k string, v string) error
	Close()
	GetAllKeys(table string) KeyValueIterator
	Len(table string) int64
	LogSimilar(url string, canonical string) error
	StartTx() Tx
}

type KeyValueIterator interface {
	Next() bool
	Current() string
	Close()
}

type Tx interface {
	End()
}
