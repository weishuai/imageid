package imageid

import (
	"bitbucket.org/taringa/imageid/config"
	"bitbucket.org/taringa/imageid/kvstore"
	"bitbucket.org/taringa/imageid/log"
	"bitbucket.org/taringa/imageid/mtree"
	"bitbucket.org/taringa/imageid/stats"
)

type DB struct {
	Store    kvstore.KeyValueStore
	hashTree *mtree.MTree
}

const TableMD5s = "md5s"
const TableHashes = "hashes"
const TableURLs = "urls"

func OpenDB() *DB {
	useNullStore := config.Env("IMAGEID_NULL_STORE", "") != ""
	var store kvstore.KeyValueStore
	if useNullStore {
		store = kvstore.NewNullStore()
	} else {
		store = kvstore.MysqlStoreOpen(TableMD5s, TableHashes, TableURLs)
	}
	return &DB{store, mtree.New()}
}

func (db *DB) Init() {
	log.Infof("Initializing DB...")
	iter := db.Store.GetAllKeys(TableHashes)
	defer iter.Close()
	for iter.Next() {
		key := iter.Current()
		depth := db.hashTree.Add(hashFromKey(key))
		stats.HashAdded(depth)
	}

	stats.Md5Added(db.Store.Len(TableMD5s))
	stats.URLEntryAdded(db.Store.Len(TableURLs))
}

func (db *DB) Close() {
	db.Store.Close()
}

func (db *DB) Query(url string) string {
	return db.getCanonicalForURL(url)
}
