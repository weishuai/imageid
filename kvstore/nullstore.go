package kvstore

import (
	"sync"
)

type NullStore struct {
	mu     sync.RWMutex
	tables map[string]map[string]string
}

func NewNullStore() *NullStore {
	return &NullStore{tables: make(map[string]map[string]string)}
}

func (ns *NullStore) Close() {}

func (ns *NullStore) Get(table string, k string) (string, error) {
	ns.mu.RLock()
	defer ns.mu.RUnlock()

	if ns.tables[table] == nil {
		return "", nil
	}
	return ns.tables[table][k], nil
}

func (ns *NullStore) Add(tx Tx, table string, k string, v string) error {
	ns.mu.Lock()
	defer ns.mu.Unlock()

	if ns.tables[table] == nil {
		ns.tables[table] = make(map[string]string)
	}
	if ns.tables[table][k] != "" {
		panic("duplicate key " + k)
	}
	ns.tables[table][k] = v
	return nil
}

type NullIterator struct {
}

func (ns *NullStore) GetAllKeys(table string) KeyValueIterator {
	return &NullIterator{}
}

func (iter *NullIterator) Close() {}

func (iter *NullIterator) Next() bool {
	return false
}

func (iter *NullIterator) Current() string {
	return ""
}

func (ns *NullStore) Len(table string) int64 {
	return int64(0)
}

type NullTx struct{}

func (ns *NullStore) StartTx() Tx {
	return &NullTx{}
}

func (tx *NullTx) End() {
}

func (ns *NullStore) LogSimilar(url string, canonical string) error {
	return nil
}
