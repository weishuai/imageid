package imhash

import (
	"bytes"
	"fmt"
	"github.com/steakknife/hamming"
	"os/exec"
	"strconv"
	"strings"
)

type ImageHash struct {
	Dhash, Phash uint64
}

func (h ImageHash) String() string {
	return fmt.Sprintf("%016x%016x", h.Dhash, h.Phash)
}

func (h ImageHash) Distance(q ImageHash) int {
	return hamming.Uint64(h.Dhash, q.Dhash) + hamming.Uint64(h.Phash, q.Phash)
}

func FromString(s string) (ImageHash, error) {
	var hash ImageHash
	dhash, err := strconv.ParseUint(s[:16], 16, 64)
	if err != nil {
		return hash, err
	}
	phash, err := strconv.ParseUint(s[16:], 16, 64)
	return ImageHash{dhash, phash}, err
}

func FromFile(filename string) (ImageHash, error) {
	cmd := exec.Command("imhash", filename)

	var stdout, stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr

	err := cmd.Run()
	if err != nil {
		s := strings.Trim(stderr.String(), " \n")
		if s != "" {
			return ImageHash{}, fmt.Errorf(s)
		}
		return ImageHash{}, err
	}

	s := strings.Trim(stdout.String(), " \n")
	if len(s) != 32 {
		return ImageHash{}, fmt.Errorf("invalid hash length")
	}
	return FromString(s)
}
