#pragma once
#include <stdlib.h>

typedef unsigned long long ulong64;

typedef struct _imhash_t {
	ulong64 dhash;
	ulong64 phash;
} imhash_t;
