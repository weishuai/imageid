package stats

import (
	"bufio"
	"bytes"
	"expvar"
	"fmt"
	"math"
	"sync"
	"sync/atomic"
	"time"
)

type avgCounter struct {
	mu                 sync.Mutex
	avg60s             float64
	movingAvgTimestamp int64
}

const counterPeriodSeconds = 60

type eventCounter struct {
	mu      sync.Mutex
	buckets [counterPeriodSeconds + 1]struct {
		count      uint64
		lastUpdate int64
	}
	currentBucket uint8
}

var _stats struct {
	expvars  *expvar.Map
	counters struct {
		urlEntries           int64
		md5Entries           int64
		hashEntries          int64
		treeDepth            int64
		treeQuerySpanPercent avgCounter
		postsDropped         eventCounter
		urlsProcessed        eventCounter
		urlQueryHits         eventCounter
		urlQueryMisses       eventCounter
		txOk                 eventCounter
		txError              eventCounter
		canonicalRequests    eventCounter
	}
}

func init() {
	_stats.expvars = expvar.NewMap("imageid")

	initUptimeStat()
	initCounter("urlEntries", &_stats.counters.urlEntries)
	initCounter("md5Entries", &_stats.counters.md5Entries)
	initCounter("hashEntries", &_stats.counters.hashEntries)

	initCounter("treeDepth", &_stats.counters.treeDepth)
	_stats.expvars.Set("treeDepthIdeal", expvar.Func(func() interface{} {
		return int64(math.Ceil(math.Log2(float64(atomic.LoadInt64(&_stats.counters.hashEntries)))))
	}))

	initAvgCounter("treeQuerySpanPercent", &_stats.counters.treeQuerySpanPercent)
	initEventCounter("postsDropped", &_stats.counters.postsDropped)
	initEventCounter("urlsProcessed", &_stats.counters.urlsProcessed)
	initEventCounter("urlQueryHits", &_stats.counters.urlQueryHits)
	initEventCounter("urlQueryMisses", &_stats.counters.urlQueryMisses)
	initEventCounter("txOk", &_stats.counters.txOk)
	initEventCounter("txError", &_stats.counters.txError)
	initEventCounter("canonicalRequests", &_stats.counters.canonicalRequests)
}

func initAvgCounter(name string, p *avgCounter) {
	_stats.expvars.Set(name+"_avg60s", expvar.Func(func() interface{} {
		p.mu.Lock()
		defer p.mu.Unlock()
		return p.avg60s
	}))
	p.movingAvgTimestamp = time.Now().UnixNano()
}

func currentBucketIdx() uint8 {
	return uint8(time.Now().Unix() % (counterPeriodSeconds + 1))
}

func initEventCounter(name string, p *eventCounter) {
	_stats.expvars.Set(name+"_60s", expvar.Func(func() interface{} {
		p.mu.Lock()
		defer p.mu.Unlock()
		currentBucket := int(currentBucketIdx())
		sum := uint64(0)
		for i := 0; i < len(p.buckets); i++ {
			_, elapsed := secondsSince(p.buckets[i].lastUpdate)
			if i != currentBucket && elapsed < counterPeriodSeconds {
				sum += p.buckets[i].count
			}
		}
		return sum
	}))
}

func initUptimeStat() {
	start := time.Now()
	_stats.expvars.Set("uptime", expvar.Func(func() interface{} {
		return time.Since(start).String()
	}))
}

func initCounter(name string, pvalue *int64) {
	_stats.expvars.Set(name, expvar.Func(func() interface{} {
		return atomic.LoadInt64(pvalue)
	}))
}

func HashAdded(depth int64) {
	atomic.AddInt64(&_stats.counters.hashEntries, 1)
	d := atomic.LoadInt64(&_stats.counters.treeDepth)
	if depth > d {
		atomic.StoreInt64(&_stats.counters.treeDepth, depth)
	}
}

func Md5Added(amount int64) {
	atomic.AddInt64(&_stats.counters.md5Entries, amount)
}

func URLEntryAdded(amount int64) {
	atomic.AddInt64(&_stats.counters.urlEntries, amount)
}

func RegisterFunc(name string, f func() interface{}) {
	_stats.expvars.Set(name, expvar.Func(f))
}

func RegisterChan(name string, f func() interface{}) {
	RegisterFunc("queue_"+name, f)
}

func SetWorker(workerID, action string) {
	s := &expvar.String{}
	s.Set(action)
	_stats.expvars.Set("worker_"+workerID, s)
}

func SetInt(k string, v int64) {
	s := &expvar.Int{}
	s.Set(v)
	_stats.expvars.Set(k, s)
}

func MovingExpAvg(value, oldValue, fdtime, ftime float64) float64 {
	alpha := 1.0 - math.Exp(-fdtime/ftime)
	return alpha*value + (1.0-alpha)*oldValue
}

func TreeQueried(nodesVisited int64) {
	amount := float64(atomic.LoadInt64(&_stats.counters.hashEntries))
	updateAvgCounter(&_stats.counters.treeQuerySpanPercent, 100*float64(nodesVisited)/amount)
}

func PostDropped() {
	updateEventCounter(&_stats.counters.postsDropped)
}

func URLProcessed() {
	updateEventCounter(&_stats.counters.urlsProcessed)
}

func URLQueried(hit bool) {
	if hit {
		updateEventCounter(&_stats.counters.urlQueryHits)
	} else {
		updateEventCounter(&_stats.counters.urlQueryMisses)
	}
}

func TxEnd(ok bool) {
	if ok {
		updateEventCounter(&_stats.counters.txOk)
	} else {
		updateEventCounter(&_stats.counters.txError)
	}
}

func CanonicalRequested() {
	updateEventCounter(&_stats.counters.canonicalRequests)
}

func updateEventCounter(p *eventCounter) {
	p.mu.Lock()
	defer p.mu.Unlock()
	currentBucket := currentBucketIdx()
	if currentBucket != p.currentBucket {
		p.currentBucket = currentBucket
		p.buckets[currentBucket].count = 0
	}
	p.buckets[currentBucket].count++
	p.buckets[currentBucket].lastUpdate = time.Now().UnixNano()
}

func secondsSince(then int64) (int64, float64) {
	now := time.Now().UnixNano()
	elapsed := float64(now-then) / float64(time.Second)
	return now, elapsed
}

func updateAvgCounter(p *avgCounter, value float64) {
	p.mu.Lock()
	defer p.mu.Unlock()

	now, step := secondsSince(p.movingAvgTimestamp)
	p.avg60s = MovingExpAvg(value, p.avg60s, step, float64(counterPeriodSeconds))
	p.movingAvgTimestamp = now
}

func JSON() string {
	var b bytes.Buffer
	w := bufio.NewWriter(&b)
	fmt.Fprintf(w, "{\n")
	first := true
	expvar.Do(func(kv expvar.KeyValue) {
		if !first {
			fmt.Fprintf(w, ",\n")
		}
		first = false
		fmt.Fprintf(w, "%q: %s", kv.Key, kv.Value)
	})
	fmt.Fprintf(w, "\n}\n")
	w.Flush()
	return b.String()
}
