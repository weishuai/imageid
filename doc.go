// Package imageid provides tools to detect similar images in a large
// collection.
package imageid
