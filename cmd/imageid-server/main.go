/*
imageid-server provides access to imageid tools using HTTP endpoints.

See the imageid/server package documentation for a description of the
available endpoints.
*/
package main

import (
	"bitbucket.org/taringa/imageid/server"
)

func main() {
	server.RunServer(nil)
}
